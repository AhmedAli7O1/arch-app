FROM node:8.5-alpine

RUN mkdir -p /app
WORKDIR /app

RUN npm install pm2 -g

COPY package.json /app/
RUN npm install

COPY . /app

CMD ["pm2-docker", "index.js"]
